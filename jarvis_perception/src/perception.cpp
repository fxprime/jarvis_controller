#include "jarvis_perception/perception.hpp"

/* Special case : manual point picker using joy stick */
#include "jarvis_body_hand/common.hpp"

namespace jarvis_perception {

Perception::Perception(ros::NodeHandle& nodeHandle,
					   std::string rgb_frame,
					   std::string robot_frame)
    : nodeHandle_(nodeHandle),
      rgb_frame_(rgb_frame),
      robot_frame_(robot_frame),
      inFrame(cvCreateImage(cvSize(640, 480), 8, 3)),
      cloud_pcl (new pcl::PointCloud<pcl::PointXYZRGB>)
{
	ROS_INFO("PERCEPTION: SETUP..");

	STOP_THREAD = false;
	REQUEST_RESET = true;
	PAUSED=false;


	SYSTEM_READY = false;

	readParameters();


	
	person_pub = nodeHandle_.advertise<geometry_msgs::Vector3>("person_position", 10);
	object_pub = nodeHandle_.advertise<geometry_msgs::Vector3>("object_position", 10);
	// vector_pub_pour = nodeHandle_.advertise<geometry_msgs::Vector3>("manipulator/pour", 10);
	vector_pub_pointcloud = nodeHandle_.advertise<sensor_msgs::PointCloud2>("object_pointcloud", 10);

	yolosub_ = nodeHandle_.subscribe("/darknet_ros/bounding_boxes", 2, &Perception::YOLOcb, this);

	/* Special case : manual point picker using joy stick */
	joysub_ = nodeHandle_.subscribe("/joy", 2, &Perception::joycb, this);

    listener = new tf::TransformListener();

    ROS_INFO("PERCEPTION: START");
}



void Perception::joycb(const sensor_msgs::Joy::ConstPtr& msg)
{
	if(msg->buttons[jarvis_body_hand::buttonAxisLeft]==1)
	{
		/* Select center of image */
		SelectPointcloud( 640/2,480/2, "object");
	}
}
void Perception::YOLOcb(const darknet_ros_msgs::BoundingBoxes::ConstPtr& msg) 
{
	YOLO = *msg;

	for(uint8_t i=0; i< YOLO.boundingBoxes.size(); i++)
	{
		if(YOLO.boundingBoxes[i].Class == "person" && YOLO.boundingBoxes[i].probability > 0.6)
		{

			double x = ( YOLO.boundingBoxes[i].xmin + YOLO.boundingBoxes[i].xmax ) * 0.5;
			double y = ( YOLO.boundingBoxes[i].ymin + YOLO.boundingBoxes[i].ymax ) * 0.5;
			SelectPointcloud( (int)x, (int)y, "person");
			break;
			//TODO(BOOM) SELECT TRUE PERSON
		}
	}

}

void Perception::Run() 
{

}

bool Perception::Stop() 
{
	STOP_THREAD=true;
	return PAUSED;
}

void Perception::RequestReset() 
{
	REQUEST_RESET = true;
	STOP_THREAD = false;
}

bool Perception::FINISH() const
{
	return true;//MotorListMsgQueue.size()==0;
}

void Perception::readParameters()
{

}






void Perception::SelectPointcloud(int x, int y, string type)
{
	//This will prevent mismatch cloud and img msg that will cause unexpected behiever (max)
	if(!SYSTEM_READY) return;

	
	_closestPoint.x = cloud_pcl->points[y*640+x].x;
	_closestPoint.y = cloud_pcl->points[y*640+x].y;
	_closestPoint.z = cloud_pcl->points[y*640+x].z;
	if( _closestPoint.x == _closestPoint.x 
		&& _closestPoint.y == _closestPoint.y
		&& _closestPoint.z == _closestPoint.z
	)
	try{

	    geometry_msgs::PointStamped kinect_point;
	    geometry_msgs::PointStamped base_point;

	    kinect_point.header.frame_id = rgb_frame_;
	    kinect_point.header.stamp = timeStamp;
	    kinect_point.point.x = _closestPoint.x;
	    kinect_point.point.y = _closestPoint.y;
	    kinect_point.point.z = _closestPoint.z;
	    listener->transformPoint(robot_frame_, kinect_point, base_point);
	    _closestPoint.x = base_point.point.x;
	    _closestPoint.y = base_point.point.y;
	    _closestPoint.z = base_point.point.z;
	    

	    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_pcl_filtered(new pcl::PointCloud<pcl::PointXYZRGB>);
	    PclProcessing(cloud_pcl, cloud_pcl_filtered);






        sensor_msgs::PointCloud2 cloud_tf_out;
        pcl::toROSMsg(*cloud_pcl_filtered, cloud_tf_out);
	    vector_pub_pointcloud.publish(cloud_tf_out);


		//ADD MORE TRANSFORM RELATION HERE
	    tf::Transform transform;
		tf::Quaternion q;
		q.setRPY(0,0,0);
		transform.setOrigin(tf::Vector3(_closestPoint.x,_closestPoint.y,_closestPoint.z));
		transform.setRotation(q);

	    if(type=="person")
	    {
	    	person_pub.publish(_closestPoint);

			static tf::TransformBroadcaster person_br;
			
			person_br.sendTransform(
			tf::StampedTransform(
			transform, ros::Time::now(), "base_link", "person"));
	    }

	    if(type=="object")
	    {
	    	object_pub.publish(_closestPoint);

			static tf::TransformBroadcaster object_br;
			
			object_br.sendTransform(
			tf::StampedTransform(
			transform, ros::Time::now(), "base_link", "object"));
	    }
		


	    
	    // std::cout << "Raw pts: x:" << kinect_point.point.x << " y : " << kinect_point.point.y << " z : " << kinect_point.point.z << std::endl; 
	    // printf("send : x:%.5f y:%.5f z:%.5f\n",vector.x,vector.y,vector.z);						
	}catch (tf::TransformException &ex) {
	  ROS_ERROR("%s",ex.what());
	  ros::Duration(0.05).sleep();
	  // continue;
	}

}



Perception::~Perception()
{
}



void Perception::depthCb(const sensor_msgs::PointCloud2& cloud)
{
  
  static int msg_count = 0;
  
  if(!SYSTEM_READY && msg_count > 20) 
  {
	SYSTEM_READY=true;
	ROS_INFO("PERCEPTION: Ready to request 3D position");
  }else{
  	++msg_count;
  }

  // ROS_INFO("PERCEPTION: CLOUD OK");
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_tmp (new pcl::PointCloud<pcl::PointXYZRGB>);
  pcl::PCLPointCloud2 PCLPointCloud_tmp;
  if ((cloud.width * cloud.height) == 0)
    return; //return if the cloud is not dense!
  try {
    pcl_conversions::toPCL(cloud, PCLPointCloud_tmp);

    //pcl::fromROSMsg(cloud, *cloud_tmp);

    pcl::fromPCLPointCloud2(PCLPointCloud_tmp, *cloud_tmp);

    //cloud_tmp->header = cloud.header;

    timeStamp = cloud.header.stamp;
    listener->waitForTransform(rgb_frame_, cloud.header.frame_id, cloud.header.stamp, ros::Duration(1.0));
    pcl_ros::transformPointCloud(rgb_frame_, *cloud_tmp, *cloud_pcl, *listener);

  } catch (std::runtime_error e) {
    ROS_ERROR_STREAM("Error message: " << e.what());
  }
}


void Perception::kinectCallBack(const sensor_msgs::ImageConstPtr& msg)
{
	int inKey = 0;
	// ROS_INFO("PERCEPTION: IMG OK");
	for(int i=0;i<640*480;i++)
	{
			inFrame->imageData[i*3] = msg->data[i*3];
			inFrame->imageData[i*3+1] = msg->data[i*3+1];
			inFrame->imageData[i*3+2] = msg->data[i*3+2];
	}
		
}




void Perception::PclProcessing(const pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_in, 
							   pcl::PointCloud<pcl::PointXYZRGB>::Ptr &cloud_out)
{
	// //LIMIT PCL Z AXIS FROM 0 TO 1.2
    pcl::PassThrough<pcl::PointXYZRGB> pass;
    pass.setInputCloud (cloud_in);
    pass.setFilterFieldName ("z");
    pass.setFilterLimits (0.0, 1.2);
    //pass.setFilterLimitsNegative (true);
    pass.filter (*cloud_out);

}





} /* namespace */
