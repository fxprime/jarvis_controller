////////////////////THIS FILE NO LONGER USE (TEST PULL FROM ANOTHER FORK)
#include <ros/ros.h>

#include <sensor_msgs/Joy.h>
#include <std_msgs/Float64.h>
#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseStamped.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>

#include <Eigen/Dense>
#include <eigen_conversions/eigen_msg.h>
#include <tf_conversions/tf_eigen.h>

#define Kv 6.0
#define Kth 1.0


/*
 * Description : Wheel based navigation and control system
 * Author      : Thanabadee Bulunseechart
 */


template<typename T>
void pop_front(std::vector<T>& vec)
{
    assert(!vec.empty());
    vec.erase(vec.begin());
}
enum
{
	x=0,
	y,
	z
};
enum
{
	RC_MODE =0,
	WP_MODE,
	NUM_MODE
};
namespace DriveBase 
{
using namespace Eigen;

class Wheel
{
	
	public:
		Wheel()
		{
			WheelOut = std::vector<double>{0,0,0,0};
			bGoalUpdated = false;
			bFINISHED = true;
			bReached_goal_pos = false;
			DRIVE_MODE = RC_MODE;
			path_idx = 0;
			bHoldYaw = false;
			bTF_UPDATED = false;
		}
		void joyCb(const sensor_msgs::Joy::ConstPtr& msg)
		{

			if(msg->axes[0]!=0){
				Vth = 3*msg->axes[0];
				bHoldYaw = false;
			}else{
				//This will effect only non-manual mode
				Vth = 0;
				bHoldYaw = true;
			}

			if(DRIVE_MODE==RC_MODE)
			{
				Vd[x] =  msg->axes[4];
				Vd[y] =  msg->axes[3];
				Vd[z] = 0;
				Vd_norm = 3*Vd.norm();
				
				dth = atan2(Vd[y], Vd[x]);

				//We shouldn't hold Yaw in manual mode, since it will depend on mapping.
				bHoldYaw = false;
			}

			if(msg->buttons[6] > 0)
			{
				DRIVE_MODE=(DRIVE_MODE+1)%NUM_MODE;
				ROS_INFO_STREAM("CHANGE MODE TO -> " << DRIVE_MODE);
			}
			// ROS_INFO_STREAM("REC->" << Vd[x] << "\t" << Vd[y] << "\t" << Vd[th]);
		}
		void pathCb(const nav_msgs::Path::ConstPtr& msg)
		{
			
			if(msg->poses.size() >0 && !bFINISHED)
			{
				PathD = *msg;
				// ROS_INFO_STREAM("Get path information size = " << PathD.poses.size());
			}
		}
		void goalCb(const geometry_msgs::PoseStamped::ConstPtr& msg)
		{
			CurGoal = *msg;
			bGoalUpdated = true;
		}
		void CheckGoalUpdate()
		{
			//Check new goal arrival, reset potential logic here
			if(bGoalUpdated)
			{
				bFINISHED = false;
				//RESET path_idx
				path_idx = 0;
				bGoalUpdated = false;
			}
		}
		void Control_system()
		{
			CheckGoalUpdate();
			GetTarget();

			//GOTO TARGET
			if(DRIVE_MODE==WP_MODE)
			{
				Vector3d diff = target.translation()-current.translation();
				Vd_norm = Kv*fabs(( diff ).norm());
				Matrix3d R = current.rotation();
				Vector3d diff_bf = R.inverse() * diff;
				dth = (atan2(diff_bf[y], diff_bf[x]));
				// ROS_INFO_STREAM("Heading -> " << dth << "diffx->" << diff_bf[x] << "diffy->" << diff_bf[y]);
			}

			//Hold yaw angle in every mode since TF still alive!
			if(bHoldYaw && bTF_UPDATED)
			{
				// ROS_INFO("%.3f",(target.rotation().eulerAngles(0,1,2))(2));
				Vth = Kth*3*((target.rotation().eulerAngles(0,1,2))(2)
					          	 -(current.rotation().eulerAngles(0,1,2))(2));
			}

			//if bfinished or TF lost, stop this machine asap!
			if(bFINISHED && DRIVE_MODE==WP_MODE || !bTF_UPDATED)
			{
				Vd_norm = 0;
				dth = 0;		
			}

		}
		std::vector<double> GetWheelOut()
		{
			const double quater_pi = 0.785398;
			// WheelOut[0] = 	Vd[x]-Vd[y]-Vth	;
			// WheelOut[1] = 	Vd[x]+Vd[y]-Vth	;
			// WheelOut[2] = -(Vd[x]+Vd[y]+Vth)	;
			// WheelOut[3] = -(Vd[x]-Vd[y]+Vth)	;
			WheelOut[0] =   Vd_norm*cos(dth+quater_pi) - Vth;
			WheelOut[1] =   Vd_norm*sin(dth+quater_pi) - Vth;
			WheelOut[2] = -(Vd_norm*sin(dth+quater_pi) + Vth);
			WheelOut[3] = -(Vd_norm*cos(dth+quater_pi) + Vth);

			
			// if(PathD.poses.begin())
			return WheelOut;
		}
		bool GetCurrentPose()
		{
			bTF_UPDATED = false;
			try{
				tf::StampedTransform Stransform;
				listener.lookupTransform("/map", "/base_link", ros::Time(0), Stransform);
				transformTFToEigen(Stransform, current);
				bTF_UPDATED = true;
				// ROS_INFO_STREAM("I get x = " << Stransform.getOrigin().x() << "\t" << Stransform.getOrigin().y());
				// ROS_INFO_STREAM("GET ROS TIME\t" << ros::Time::now() << "\t" << Stransform.stamp_);
				return true;
			}
			catch (tf::TransformException &ex) {
			  ROS_ERROR_THROTTLE(3, "Cannot get current pose, Please open mapping before using drive_base..");
			  ros::Duration(0.05).sleep();
			  // continue;
			  return false;
			}
		}
		bool CheckRadius(geometry_msgs::Pose pose, double radius)
		{	
			Affine3d next;
			tf::poseMsgToEigen( pose, next );
			double distance = fabs((next.translation()-current.translation()).norm());
			bool checker = (distance>radius ? false:true);
			return	checker;
		}

		void GetTarget()
		{
			if(DRIVE_MODE==RC_MODE)
			{
				target.translation()= current.translation();
				if(!bHoldYaw)
					target.linear() = current.rotation();
				return;
			}
			//TODO(MAX) REMOVE juggling beheiveor when the new obstruct is observed

			/*  Check radius goal point is in robot base?
			 *  if yes    -> use that target 
			 *  if no     -> check next path point that outside radius and use that target
			 *  if robot is exactly or small radius of goal then turn on FINISH
			 */
			bFINISHED = CheckRadius(CurGoal.pose, 0.05);
			
			if(bFINISHED) 
			{
				ROS_INFO_THROTTLE(3, "FINISHED ! ready to next command");
				//RESET path_idx
				path_idx = 0;
			}
			
			if(!bFINISHED)
			{
				//FIND TARGET
				if(!CheckRadius(CurGoal.pose, 0.3) && PathD.poses.size()!=0)
				{
					//Assign translation
					tf::poseMsgToEigen( PathD.poses[path_idx].pose, target );

					//Assign rotation
					//TODO(MAX) : add choice for path following heading or ROI
					Vector3d diff = target.translation()-current.translation();
					double YawTarget = (atan2(diff[y], diff[x]));
					Quaterniond yawq(cos(YawTarget / 2), 0, 0, sin(YawTarget / 2));
					yawq.normalize();
					target.linear() = yawq.toRotationMatrix();

					if(CheckRadius(PathD.poses[path_idx].pose, 0.3) && PathD.poses.size() > path_idx+1)
					{
						++path_idx;
						// ROS_INFO("%d", path_idx);
					}
				}else{
					tf::poseMsgToEigen( CurGoal.pose, target );
				}
			}
		}

	private:
		Vector3d Vd;
		double Vd_norm;
		double Vth;
		double dth;
		std::vector<double> WheelOut;
		nav_msgs::Path PathD;
		geometry_msgs::PoseStamped CurGoal;
		bool bGoalUpdated;
		bool bFINISHED;
		tf::TransformListener listener;
		Affine3d target;
		Affine3d current;
		bool bReached_goal_pos;
		int DRIVE_MODE;
		bool bHoldYaw;
		int path_idx;
		bool bTF_UPDATED;
};
};
using namespace DriveBase;
using namespace Eigen;
int main(int argc, char** argv)
{
	ros::init(argc, argv, "jarvis_drive_test");
	ros::NodeHandle nodeHandle_("~");
	//INITIALIZE VARIABLE
	Wheel Drive;
	ros::Subscriber subJoy_ = nodeHandle_.subscribe("/joy", 1,
                                      &Wheel::joyCb, &Drive);
	ros::Subscriber subPath_ = nodeHandle_.subscribe("/move_base_node/TrajectoryPlannerROS/global_plan", 1,
									  &Wheel::pathCb, &Drive);
	ros::Subscriber subGoal_ = nodeHandle_.subscribe("/move_base_node/current_goal", 10,
									  &Wheel::goalCb, &Drive);
	std::vector<ros::Publisher> pubMot;
	for(uint8_t j=0; j<4; j++)
	      pubMot.push_back(nodeHandle_.advertise<std_msgs::Float64>
	      	("/wheel" + std::to_string(j) + "_controller/command", 10));
	ROS_INFO("Wheel test: Successful launch");

	ros::Rate r(10);

	while(ros::ok())
	{

		Drive.GetCurrentPose();
		Drive.Control_system();
		// Drive.CheckReach();

		std::vector<double> WheelOut = Drive.GetWheelOut();
		for(uint8_t i=0; i<4; i++)
		{
			std_msgs::Float64 WheelMsgs;
			WheelMsgs.data = WheelOut[i];
			pubMot[i].publish(WheelMsgs);
		}
		// ROS_INFO_STREAM("Drive->" << WheelOut[0] << "\t" << WheelOut[1] << "\t" << WheelOut[2] << "\t" << WheelOut[3] );

		r.sleep();
		ros::spinOnce();
	}
	
	return 0;
}





