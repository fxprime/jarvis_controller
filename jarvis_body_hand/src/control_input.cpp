#include "jarvis_body_hand/control_input.hpp"
/*
 * Description : Control inpu of jarvis 
 * Author      : Thanabadee Bulunseechart
 */

namespace jarvis_body_hand {

ControlInput::ControlInput(ros::NodeHandle& nodeHandle,
													 Voice* voice)
    : nodeHandle_(nodeHandle),
      voice_(voice)
{
	CONTROL_HEAD_SENSE=0.05;
	RC.axes.resize(Naxes);
	RC.buttons.resize(Nbuttons);
	DRIVE_MODE=RC_MODE;
	HEAD_MODE =HEAD_MANUAL;
	readParameters();
	subJoy_ = nodeHandle_.subscribe("/joy", 1,
                                      &ControlInput::joyCb, this);
}

ControlInput::~ControlInput()
{
	delete voice_;
}

void ControlInput::joyCb(const sensor_msgs::Joy::ConstPtr& msg)
{
	RC = *msg;
	ModeManager();
}

void ControlInput::ModeManager()
{
	if(RC.buttons[buttonSEL] > 0)
	{
		DRIVE_MODE=(DRIVE_MODE+1)%NUM_MODE;
		ROS_INFO_STREAM("ControlInput : CHANGE MODE TO -> " << DRIVE_MODE);

		switch (DRIVE_MODE) {
			case RC_MODE:voice_->Talk("Manual Mode");break;
			case WP_MODE:voice_->Talk("Waypoint Mode");break;
			default:break;
		}
		
	}
	if(RC.buttons[buttonLOGITECH] >0)
	{
		HEAD_MODE=(HEAD_MODE+1)%NUM_HEAD_MODE;
		ROS_INFO_STREAM("ControlInput : CHANGE HEAD MODE TO -> " << HEAD_MODE);

		switch (HEAD_MODE) {
			case HEAD_MANUAL:					voice_->Talk("Head Manual Mode");break;
			case HEAD_FOLLOW_PERSON:	voice_->Talk("Head follow person Mode");break;
			case HEAD_FOLLOW_OBJECT:	voice_->Talk("Head follow object Mode");break;
			case HEAD_GUIDE:					voice_->Talk("Head guide Mode");break;
			default:break;
		}
		
	}


}

void ControlInput::GetHeadControl(double head_vel_control[2])
{
	head_vel_control[0] = CONTROL_HEAD_SENSE*RC.axes[ARH];
	head_vel_control[1] = CONTROL_HEAD_SENSE*RC.axes[ARV];
}



void ControlInput::readParameters()
{
	nodeHandle_.getParam("/CONTROL_HEAD_SENSE", CONTROL_HEAD_SENSE);

}


} /* namespace */
