#include "jarvis_body_hand/System.hpp"

// STD
#include <string>

#include <dynamixel_msgs/MotorStateList.h>
#include <control_msgs/FollowJointTrajectoryFeedback.h>
#include <trajectory_msgs/JointTrajectoryPoint.h>
#include <std_msgs/Float64.h>
#include <std_msgs/String.h>






/*  GRUOP         [right_hand, left_hand]
 *  START_JOINT   [0, 5]
 *  NUMJOINT      [5]
 *  JOINT_IK_DIR  [-1, -1, 1, -1, -1, -1, -1, 1, -1, -1]
 *  JOINT_SCALE   [0.00506145483, 0.00104719755, 0.00104719755, 0.00506145483,,,,,]
 *  JOINT_RAW_HOME[507, 2020, 2077, 504],,,,,,,
 *  
 *  
 */






namespace jarvis_body_hand {
using namespace Eigen;
System::System(ros::NodeHandle& nodeHandle)
    : nodeHandle_(nodeHandle)
{
  if (!readParameters()) {
    ROS_ERROR("Could not read parameters. Check size of parameters. Please use roslaunch to include param.");
    ros::requestShutdown();
  }

  

  //Initialize the voice class
  voice_ = new Voice(nodeHandle_);

  //Initialize the control input class
  controlin_ = new ControlInput(nodeHandle_, voice_);

  //Initialize the control input class
  kinetic_ = new Kinematic(nodeHandle_, "right_arm", /*"right_hand_link"*/ "right_hand_dummy2", voice_);
  kinetic_->Debug(false);

  //Initialize the actuator thread and launch
  actuator_ = new Actuator(nodeHandle_, controlin_, voice_);
  t_actuator = new std::thread(&jarvis_body_hand::Actuator::Run,actuator_);


  //Initialize the Record hand thread and launch
  rec_hand_ = new Record_hand(nodeHandle_, actuator_);
  t_rec_hand = new std::thread(&jarvis_body_hand::Record_hand::Run,rec_hand_);

  //Initialize the actuator thread and launch
  drivebase_ = new DriveBase(nodeHandle_, controlin_, actuator_, voice_);
  t_drivebase = new std::thread(&jarvis_body_hand::DriveBase::Run,drivebase_);

  //Initialize the thread and launch
  cmd_ = new Command(nodeHandle_, voice_, controlin_, kinetic_, actuator_, rec_hand_, drivebase_);
  t_cmd = new std::thread(&jarvis_body_hand::Command::Run, cmd_);

  sub_object_position_ = nodeHandle_.subscribe("/jarvis_perception/object_position", 1,
                                      &System::ObjectPositionCallback, this);

  sub_person_position_ = nodeHandle_.subscribe("/jarvis_perception/person_position", 1,
                                      &System::PersonPositionCallback, this);


  sub_Rviz_ = nodeHandle_.subscribe("/joint_states", 1,
                                      &System::RvizCallback, this);

  //TODO
  serviceServer_ = nodeHandle_.advertiseService("get_average",
                                                &System::serviceCallback, this);



  //Initialize
  ROS_INFO("Successfully launched node.");
  voice_->Talk("Successfully launched node.");


  //Record TODO 1. send nodehandle for publish on playback 
  //Careful for publish conflict (output to motor) should switch by mode
  //Service for change speed and slope

    // sleep(5);
    // std_msgs::String talk;
    // talk.data = "hello";
    // pubTTS.publish(talk);
    // sleep(0.5);
    // pubTTS.publish(talk);
    // sleep(0.5);
    // pubTTS.publish(talk);

    // talk.data = "hello";
    // rec_hand_->LoadBag(talk.data);
    // pubTTS.publish(talk);
    // // rec_hand_->Stop();
    // while(!rec_hand_->FINISH() && ros::ok()) {
    //   sleep(0.1);
    // }

    // talk.data = "dance";
    // rec_hand_->LoadBag(talk.data);
    // pubTTS.publish(talk);
    // // rec_hand_->Stop();
    // while(!rec_hand_->FINISH() && ros::ok()) {
    //   sleep(0.1);
    // }

    // talk.data = "show";
    // rec_hand_->LoadBag(talk.data);
    // pubTTS.publish(talk);
    // // rec_hand_->Stop();
    // while(!rec_hand_->FINISH() && ros::ok()) {
    //   sleep(0.1);
    // }

    // talk.data = "bye bye";
    // rec_hand_->LoadBag(talk.data);
    // pubTTS.publish(talk);
    // // rec_hand_->Stop();
    // while(!rec_hand_->FINISH() && ros::ok()) {
    //   sleep(0.1);
    // }


    // while(1)
    // {
    //   sleep(1);
    // }



  ros::AsyncSpinner spinner(4);
  spinner.start();


  // //Minimum load and prepare
  // kinetic_->MoveArmTo(Vector3d(0.07, RIGHT_HAND_Y, 0.4),
  //                    Vector3d(0,-1.57,0));
  // sleep(3.0);
  // Vector3d tran( 0.0,RIGHT_HAND_Y,0.2755);
  // Vector3d rot(  0,0,0);
  // kinetic_->MoveArmTo(tran,rot);

  // sleep(4.0);

  //Add StupidTable at,pose,size,name
  // kinetic_->AddObj(Vector3d(0.49, -0.05, 0.43-0.24/2),
  //                 Vector3d(0,     0,         0),
  //                 Vector3d(0.4,   1,        0.68),
  //                 "box");
 


  while(ros::ok()) {
    if(controlin_->RC.buttons[buttonA])
    {
      kinetic_->setHome();
      kinetic_->ResetOctomap();
    }else if(controlin_->RC.buttons[buttonY])
    {
      PrepareGrip();
    }
    sleep(0.5);
    tf::StampedTransform Stransform;
    // actuator_->setObjectOfInterest(OBJECT);
    // bool found = actuator_->LookingFor(OBJECT, Stransform);

    // if(found)
    // {
    //   Eigen::Quaterniond q_init;
    //   Eigen::Vector3d v_init;
    //   tf::quaternionTFToEigen(Stransform.getRotation(), q_init);
    //   tf::vectorTFToEigen(Stransform.getOrigin(), v_init);

    //   GripAt(Stransform.getOrigin().x(),
    //          Stransform.getOrigin().y(), 
    //          Stransform.getOrigin().z()-0.05);
    //   PlaceAt(0.4, -0.15, 0.70);
    // }else{
    //   ROS_ERROR("OBJECT not found");
    // }
    


      // GripAt(0.45, 0, 0.70);
      // PlaceAt(0.3, 0.3, 0.70);
      // GripAt(0.3, 0.3,0.70);
      // PlaceAt(0.3, -0.3, 0.70);
      // GripAt(0.3, -0.3,0.70);
      // PlaceAt(0.4, -0.15, 0.70);
      // GripAt(0.4, -0.15, 0.70);
      // PlaceAt(0.4, 0.185, 0.70);
      // GripAt(0.4, 0.185, 0.70);
      // PlaceAt(0.45, 0, 0.70);


  }

}

System::~System()
{
  delete rec_hand_;
  delete t_rec_hand;

  delete kinetic_;

  delete actuator_;
  delete t_actuator;

  delete controlin_;

  delete voice_;
}



bool System::PrepareGrip()
{
  //Prepare grip
  bool success = kinetic_->MoveArmTo(Vector3d(0.30, RIGHT_HAND_Y, 0.83),
                                    Vector3d(0,-2.571,0));
  sleep(1.0);
  return success;
}

bool System::GripAt(double x, double y, double z)
{
  PrepareGrip();
  if(!kinetic_->GrapAt(Vector3d(x,/*RIGHT_HAND_Y+*/y,z),
                  Vector3d(0,-1.571,0))) 
    return false;
  sleep(2.0);
  if(!kinetic_->MoveArmTo(Vector3d(x,RIGHT_HAND_Y+y,z+0.05),
                  Vector3d(0,-1.571,0), true)) 
    return false;
  sleep(2.0);
  return true;
}

bool System::PlaceAt(double x, double y, double z)
{
  PrepareGrip();
  if(!kinetic_->PlaceAt(Vector3d(x,/*RIGHT_HAND_Y+*/y,z),
                  Vector3d(0,-1.571,0))) 
    return false;
  sleep(2.0);
  if(!kinetic_->MoveArmTo(Vector3d(x,RIGHT_HAND_Y+y,z+0.05),
                  Vector3d(0,-1.571,0), true)) 
    return false;
  sleep(2.0);
  return true;
}

bool System::GripAt(Vector3d v) {  
  return GripAt(v(0), v(1), v(1)); }
bool System::PlaceAt(Vector3d v) { 
  return PlaceAt(v(0), v(1), v(1)); }











bool System::readParameters()
{
  nodeHandle_.getParam("/OBJECT", OBJECT);
  if(!nodeHandle_.getParam("/JOINT_N", JOINT_N) ) 
    return false;
  nodeHandle_.getParam("/JOINT_IK_DIR", JOINT_IK_DIR);
  nodeHandle_.getParam("/JOINT_RVIZ", JOINT_RVIZ);
  nodeHandle_.getParam("/JOINT_START_IDX", JOINT_START_IDX);
  nodeHandle_.getParam("/JOINT_GROUP", JOINT_GROUP);
  if(JOINT_IK_DIR.size() != JOINT_RVIZ.size() ||
     JOINT_GROUP.size()  != JOINT_START_IDX.size())
    return false;
  return true;
}



void System::RvizCallback(const sensor_msgs::JointState::ConstPtr &message)
{
  //TODO (MAX) do it with for, we should modular it!
  //Grip open = 0.6 Grip close = 0
  std::vector<double> GripStates;
  double GS = kinetic_->GetGripState();

  GripStates.push_back(GS);

  // pubGrip0.publish(GripStates[0]);

  kinetic_->setJointAngleModel(message);
  actuator_->setRawMot(message);
  actuator_->setGripper(GripStates);

}



void System::ObjectPositionCallback(const geometry_msgs::Vector3::ConstPtr &msg)
{

  /* If robot didn't have any object in its hand then command is add object to server then grab it 
   * Otherwise, just place it over table position 10 cm
   */
  if(!kinetic_->arm_has_object)
  {
    voice_->Talk("Adding Object to scene");
    kinetic_->AddObj(Vector3d(msg->x, msg->y, msg->z-0.03),
                Vector3d(0,     0,         0),
                Vector3d(0.02,   0.02,        0.18/*+0.03*/),
                "box");
    ROS_INFO_STREAM(msg->x << "\t" << msg->y << "\t" << msg->z);

    /* Check if simulator (pToJ) is ok then try to grip object */
    if(kinetic_->pToJ(Vector3d(msg->x-0.03, msg->y, msg->z-0.05), Vector3d(0,-1.571,0)))
      GripAt(msg->x-0.03, msg->y, msg->z-0.05);
  }
  else
  {
    if(kinetic_->pToJ(Vector3d(msg->x-0.03, msg->y, msg->z+0.1), Vector3d(0,-1.571,0)))
      PlaceAt(msg->x-0.03, msg->y, msg->z+0.1);
  }


  PrepareGrip();
  // voice_->Talk("Going back to home position");
  // kinetic_->setHome();
  kinetic_->ResetOctomap();

}
void System::PersonPositionCallback(const geometry_msgs::Vector3::ConstPtr &msg)
{


  actuator_->setPersonOfInterest("person");

}


bool System::serviceCallback(std_srvs::Trigger::Request& request,
                                         std_srvs::Trigger::Response& response)
{
  // response.success = true;
  // response.message = "The average is " + std::to_string(algorithm_.getAverage());
  return true;
}

} /* namespace */
