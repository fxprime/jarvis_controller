namespace jarvis_body_hand {

	#define constrain(amt,low,high) ((amt)<(low)?(low):((amt)>(high)?(high):(amt)))


	//RC axis mapping
	/*
		ARH = arrow horizon
		ARV = arrow vertical
	*/
	enum 
	{
		RUD=0,
		THT,
		LT,
		AIL,
		ELE,
		RT,
		ARH,
		ARV,
		Naxes
	};
	//RC button mapping
	/*

	*/
	enum 
	{
		buttonA=0,
		buttonB,
		buttonX,
		buttonY,
		buttonLB,
		buttonRB,
		buttonSEL,
		buttonSTART,
		buttonLOGITECH,
		buttonAxisLeft,
		buttonAxisRight,
		Nbuttons
	};

	template<typename T>
	void pop_front(std::vector<T>& vec)
	{
	    assert(!vec.empty());
	    vec.erase(vec.begin());
	}
	enum
	{
		x=0,
		y,
		z
	};
	enum
	{
		RC_MODE =0,
		WP_MODE,
		NUM_MODE
	};

	enum 
	{
		HEAD_MANUAL=0,
		HEAD_FOLLOW_PERSON,
		HEAD_FOLLOW_OBJECT,
		HEAD_GUIDE,
		NUM_HEAD_MODE
	};

};