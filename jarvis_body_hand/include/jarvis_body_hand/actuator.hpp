#pragma once
/*
 * Description : Dynamixel motors handler and publish its tf (additionnal, PID object tracking, object grouping)
 * Author      : Thanabadee Bulunseechart
 */

#include <ros/ros.h>
#include <ros/package.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>

#include "jarvis_body_hand/control_input.hpp"
#include "jarvis_body_hand/voice.hpp"

#include <dynamixel_msgs/MotorStateList.h>
#include <dynamixel_msgs/MotorState.h>
#include <control_msgs/FollowJointTrajectoryFeedback.h>
#include <trajectory_msgs/JointTrajectoryPoint.h>
#include <dynamixel_msgs/JointState.h>
#include <std_msgs/Float64.h>
#include <sensor_msgs/JointState.h>


#ifdef USE_SOUND_LOCALIZATION
  #include <hark_msgs/HarkSource.h>
#endif

namespace jarvis_body_hand {

/*!
 * Class containing the algorithmic part of the package.
 */
class Actuator
{

 private:
  //! Internal variable to hold the current average.
  //! ROS node handle.
  ros::NodeHandle& nodeHandle_;
  //! ROS topic publisher.
  std::vector<ros::Publisher> pubMot;
  ros::Subscriber subMotAll_;

  #ifdef USE_SOUND_LOCALIZATION
    ros::Subscriber subSoundLoc_;
    void SoundLocCallback(const hark_msgs::HarkSource::ConstPtr& msg);
  #endif


  bool STOP_THREAD;
  bool PAUSED;
  bool REQUEST_RESET;

  std::vector<double> JOINT_SCALE;
  std::vector<double> JOINT_RAW_HOME;
  std::vector<int> JOINT_START_IDX;
  std::vector<std::string> JOINT_GROUP;
  std::vector<int> JOINT_IK_DIR;
  std::vector<int> JOINT_RVIZ;
  int JOINT_N;
  std::vector<int> HEAD_IDX;
  std::vector<int> WHEEL_IDX;
  double HEAD_SEARCHING_SPEED;


  std::vector<double> head_joint_pos;
  std::vector<double> head_joint_goal;


  int OBJECT_LAST_ID;
  std::vector<int> OBJECT_ID;
  std::vector<std::string> OBJECT_NAME;


  control_msgs::FollowJointTrajectoryFeedback::ConstPtr joint_states_;
  bool JOINT_STATES_UPDATED_;
  std::string object_name_;
  std::string person_name_;

  tf::TransformListener listener;

  ControlInput* controlin_;

  Voice* voice_;

  uint8_t HEAD_CONTROL_MODE;

  bool _verbose = false;

  void TF_head_process();
  void AllMotorCallback(const control_msgs::FollowJointTrajectoryFeedback::ConstPtr &message);
  void SearchingForSometing(ros::Time last_stamp);
  void readParameters();

  int GetObjectIdx(const std::string& object_name);
 public:
  
  bool updated;


  /*!
   * Constructor.
   */
  Actuator(ros::NodeHandle& nodeHandle, ControlInput* controlin, Voice* voice);

  void Run();
  bool Stop();
  void RequestReset();
  void setRawMot(const sensor_msgs::JointState::ConstPtr& message);
  void setRawMotALL(const dynamixel_msgs::MotorStateList::ConstPtr& motorstates);
  void playback(const dynamixel_msgs::MotorStateList::ConstPtr& motorstates);
  void setGripper(std::vector<double> gripper_state);
  void setHead(std::vector<double> head);
  void setWheel(std::vector<double> wheel);
  void getWheel(std::vector<double> &wheelOut);
  void getArm(std::vector<double> &armOut);
  void getHead(std::vector<double> &headOut);

  bool LookingFor(const std::string& object_name, tf::StampedTransform& Stransform);
  bool TrackingFor(const std::string& object_name, tf::StampedTransform& Stransform);
  void PIDTrackingRelativePose(tf::Vector3 relative_pose);
  void setObjectOfInterest(const std::string& object_name);
  void setPersonOfInterest(const std::string& object_name);
  bool LookingForPerson();
  bool GetPersonGoal(tf::Vector3& persongoal_out);

  void setDebug(bool enable);
  
  /*!
   * Destructor.
   */
  virtual ~Actuator();





  
};

} /* namespace */
